﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using NHibernate.Criterion;
using bdmirror24.DAL.DALFunctions.Implementations;
using bdmirror24.DAL.mappings;

namespace bdmirror24.DAL.DAL_Functions.Implementations
{
    public class ImageDAO : BaseDAORepository<bdmirrorDB.images>
    {
        public IList<bdmirrorDB.images> GetImagesbyNewsID(bdmirrorDB.news newsid)
        {
            Connection nb = new Connection();
            IList< bdmirrorDB.images> list = new List<bdmirrorDB.images>();
            using (ISession session = nb.OpenSession())
            {
                session.BeginTransaction();
                list = session.CreateCriteria(typeof(bdmirrorDB.images))
                                      .Add(Restrictions.Eq("news_id", newsid))
                                      .List<bdmirrorDB.images>();

                session.Transaction.Commit();
            }
            nb.CloseSession();
            return list;
        }
        public bdmirrorDB.images GetImagebyNewsID(bdmirrorDB.news newsid)
        {
            Connection nb = new Connection();
            bdmirrorDB.images list = new bdmirrorDB.images();
            using (ISession session = nb.OpenSession())
            {
                session.BeginTransaction();
                list = session.CreateCriteria(typeof(bdmirrorDB.images))
                                      .Add(Restrictions.Eq("news_id", newsid))
                                      .List<bdmirrorDB.images>().FirstOrDefault();

                session.Transaction.Commit();
            }
            nb.CloseSession();
            return list;
        }
    }
}
