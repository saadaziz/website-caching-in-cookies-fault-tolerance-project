﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;
using NHibernate;
using NHibernate.Criterion;
using bdmirror24.DAL.mappings;
using bdmirror24.DAL.DALFunctions.Implementations;
using bdmirror24.DAL.DALFunctions.Interfaces;
namespace bdmirror24.DAL.DAL_Functions.Implementations
{
    public class NewsDAO : BaseDAORepository<bdmirrorDB.news>
    {
        public bdmirrorDB.news getbyID(int id)
        {
            return pickbyID(id);
        }
        public bdmirrorDB.news getLastNewsbyCat(bdmirrorDB.categories cat)
        {
            bdmirrorDB.news res = null;
            try
            {
                using (ISession session = new Connection().OpenSession())
                {
                    res = session
                        .CreateCriteria(typeof(bdmirrorDB.news))
                        .Add(Restrictions.Eq("category_id", cat))
                        .AddOrder(Order.Desc("insert_time"))
                        .SetMaxResults(1)
                        .List<bdmirrorDB.news>()[0];
                }
                new Connection().CloseSession();

                return res;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public IList<bdmirrorDB.news> getAllNewsbyCatName(bdmirrorDB.categories cat)
        {
            IList<bdmirrorDB.news> res = new List<bdmirrorDB.news>();
            try
            {
                using (ISession session = new Connection().OpenSession())
                {
                    res = session
                        .CreateCriteria(typeof(bdmirrorDB.news))
                        .Add(Restrictions.Eq("cat_id", cat))
                        .List<bdmirrorDB.news>();
                }
                new Connection().CloseSession();

                return res;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public IList<bdmirrorDB.news> getAllNewsbyNewsTitle(string title)
        {
            IList<bdmirrorDB.news> res = new List<bdmirrorDB.news>();
            try
            {
                using (ISession session = new Connection().OpenSession())
                {
                    res = session
                        .CreateCriteria(typeof(string))
                        .Add(Restrictions.Eq("title_en",title))
                        .List<bdmirrorDB.news>();
                }
                new Connection().CloseSession();
                return res;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public IList<bdmirrorDB.news> getAllNewsbyCountry(bdmirrorDB.admcountry country)
        {
            IList<bdmirrorDB.news> res = new List<bdmirrorDB.news>();
            try
            {
                using (ISession session = new Connection().OpenSession())
                {
                    res = session
                        .CreateCriteria(typeof(bdmirrorDB.news))
                        .Add(Restrictions.Eq("country_id", country))
                        .List<bdmirrorDB.news>();
                }
                new Connection().CloseSession();

                return res;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public IList<int> getHomeNews(string language)
        {
            string sql = string.Empty;
            if (language != "bn")
            {
                sql =
                    @"(SELECT id from news where category_id=1 and published='1' and title_en!='' order by  news.date desc limit 0,5) UNION (SELECT id from news where category_id=2  and published='1' and title_en!='' order by  news.date desc limit 0,5) UNION (SELECT id from news where category_id=3  and published='1' and title_en!='' order by  news.date desc limit 0,5) UNION (SELECT id from news where category_id=4  and published='1' and title_en!='' order by  news.date desc limit 0,5) UNION (SELECT id from news where category_id=5  and published='1' and title_en!='' order by  news.date desc limit 0,5) UNION (SELECT id from news where category_id=6  and published='1' and title_en!='' order by  news.date desc limit 0,5) UNION (SELECT id from news where category_id=7  and published='1' and title_en!='' order by  news.date desc limit 0,5) UNION (SELECT id from news where category_id=8  and published='1' and title_en!='' order by  news.date desc limit 0,5) UNION (SELECT id from news where category_id=9  and published='1' and title_en!='' order by  news.date desc limit 0,5) UNION (SELECT id from news where category_id=10  and published='1' and title_en!='' order by  news.date desc limit 0,5) UNION (SELECT id from news where category_id=11  and published='1' and title_en!='' order by  news.date desc limit 0,5) UNION (SELECT id from news where category_id=12  and published='1' and title_en!='' order by  news.date desc limit 0,5) UNION (SELECT id from news where category_id=13  and published='1' and title_en!='' order by  news.date desc limit 0,5) UNION (SELECT id from news where category_id=14  and published='1' and title_en!='' order by  news.date desc limit 0,5) UNION (SELECT id from news where category_id=15  and published='1' and title_en!='' order by  news.date desc limit 0,5) UNION (SELECT id from news where category_id=16  and published='1' and title_en!='' order by  news.date desc limit 0,5) UNION (SELECT id from news where category_id=17  and published='1' and title_en!='' order by  news.date desc limit 0,5);";
            }
            else
            {
                sql = @"(SELECT id from news where category_id=1 and published='1' and title_bn!="" order by  news.date desc limit 0,5)
                    UNION
                    (SELECT id from news where category_id=2  and published='1' and title_bn!="" order by  news.date limit 0,5)
                    UNION
                    (SELECT id from news where category_id=3  and published='1' and title_bn!="" order by  news.date limit 0,5)
                    UNION
                    (SELECT id from news where category_id=4  and published='1' and title_bn!="" order by  news.date limit 0,5)
                    UNION
                    (SELECT id from news where category_id=5  and published='1' and title_bn!="" order by  news.date limit 0,5)
                    UNION
                    (SELECT id from news where category_id=6  and published='1' and title_bn!="" order by  news.date limit 0,5)
                    UNION
                    (SELECT id from news where category_id=7  and published='1' and title_bn!="" order by  news.date limit 0,5)
                    UNION
                    (SELECT id from news where category_id=8  and published='1' and title_bn!="" order by  news.date limit 0,5)
                    UNION
                    (SELECT id from news where category_id=9  and published='1' and title_bn!="" order by  news.date limit 0,5)
                    UNION
                    (SELECT id from news where category_id=10  and published='1' and title_bn!="" order by  news.date limit 0,5)
                    UNION
                    (SELECT id from news where category_id=11  and published='1' and title_bn!="" order by  news.date limit 0,5)
                    UNION
                    (SELECT id from news where category_id=12  and published='1' and title_bn!="" order by  news.date limit 0,5)
                    UNION
                    (SELECT id from news where category_id=13  and published='1' and title_bn!="" order by  news.date limit 0,5)
                    UNION
                    (SELECT id from news where category_id=14  and published='1' and title_bn!="" order by  news.date limit 0,5)
                    UNION
                    (SELECT id from news where category_id=15  and published='1' and title_bn!="" order by  news.date limit 0,5)
                    UNION
                    (SELECT id from news where category_id=16  and published='1' and title_bn!="" order by  news.date limit 0,5)
                    UNION
                    (SELECT id from news where category_id=17  and published='1' and title_bn!="" order by  news.date limit 0,5)";
            }
            string MyConString = new Connection().connectionString;
            MySqlConnection connection = new MySqlConnection(MyConString);

            IList<int> res = new List<int>();
            //Int32 log = 0;
            MySqlCommand command = connection.CreateCommand();
            MySqlDataReader Reader;
            //DBNull gg = null;
            command.CommandText = sql;
            connection.Open();
            Reader = command.ExecuteReader();
            while (Reader.Read())
            {
                if ((Reader["id"]) != DBNull.Value)
                {

                    res.Add(int.Parse((Reader["id"]).ToString()));
                }
                //break;

            }
            Reader.Close();
            connection.Close();

            //res = FindByQuery(sql);
            return res;

        }
        public IList<int> getMainNews(string language)
        {
            string sql = string.Empty;
            if (language != "bn")
            {
                sql =
                    @"SELECT id from news where title_en!='' and top_news='1' ORDER BY date limit 0,8;";
            }
            else
            {
                sql = @"SELECT id from news where title_bn!='' and top_news='1' ORDER BY date limit 0,8";
            }
            string MyConString = new Connection().connectionString;
            MySqlConnection connection = new MySqlConnection(MyConString);

            IList<int> res = new List<int>();
            //Int32 log = 0;
            MySqlCommand command = connection.CreateCommand();
            MySqlDataReader Reader;
            //DBNull gg = null;
            command.CommandText = sql;
            connection.Open();
            Reader = command.ExecuteReader();
            while (Reader.Read())
            {
                if ((Reader["id"]) != DBNull.Value)
                {

                    res.Add(int.Parse((Reader["id"]).ToString()));
                }
                //break;

            }
            Reader.Close();
            connection.Close();

            //res = FindByQuery(sql);
            return res;

        }
        public IList<int> getRightNews(string language)
        {
            string sql = string.Empty;
            sql = @"(select id from news where category_id=3 and published='1' order by date limit 0,5)
                    UNION
                    (select id from news where category_id=1 and published='1' order by date limit 0,5)
                    UNION
                    (select id from news where category_id=6 and published='1' order by date limit 0,5)
                    UNION
                    (select id from news where category_id=2 and published='1' order by date limit 0,5)";
          
            string MyConString = new Connection().connectionString;
            MySqlConnection connection = new MySqlConnection(MyConString);

            IList<int> res = new List<int>();
            //Int32 log = 0;
            MySqlCommand command = connection.CreateCommand();
            MySqlDataReader Reader;
            //DBNull gg = null;
            command.CommandText = sql;
            connection.Open();
            Reader = command.ExecuteReader();
            while (Reader.Read())
            {
                if ((Reader["id"]) != DBNull.Value)
                {

                    res.Add(int.Parse((Reader["id"]).ToString()));
                }
                //break;

            }
            Reader.Close();
            connection.Close();

            //res = FindByQuery(sql);
            return res;
        }

        public IList<int> getAllnewsint(int limit)
        {
            string sql = string.Empty;
            sql = "select id from news where published='1' order by date limit 0,"+limit;
            string MyConString = new Connection().connectionString;
            MySqlConnection connection = new MySqlConnection(MyConString);

            IList<int> res = new List<int>();
            //Int32 log = 0;
            MySqlCommand command = connection.CreateCommand();
            MySqlDataReader Reader;
            //DBNull gg = null;
            command.CommandText = sql;
            connection.Open();
            Reader = command.ExecuteReader();
            while (Reader.Read())
            {
                if ((Reader["id"]) != DBNull.Value)
                {

                    res.Add(int.Parse((Reader["id"]).ToString()));
                }
                //break;

            }
            Reader.Close();
            connection.Close();

            //res = FindByQuery(sql);
            return res;
        }
    }
}
