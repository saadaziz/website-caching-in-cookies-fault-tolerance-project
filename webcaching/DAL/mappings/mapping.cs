﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace bdmirror24.DAL.mappings
{
    public class categoriesmap : ClassMap<bdmirrorDB.categories>
    {
        public categoriesmap()
        {
            Id(x => x.cat_id).Column("id");
            Map(x => x.name_bn).Column("name_bn").Nullable();
            Map(x => x.name_en).Column("name_en").Nullable();
            Table("categories");
        }
        public class newsmap : ClassMap<bdmirrorDB.news>
        {
            public newsmap()
            {
                Id(x => x.news_id).Column("id");
                Map(x => x.title_bn).Column("title_bn").Nullable();
                Map(x => x.title_en).Column("title_en").Nullable();
                Map(x => x.date).Column("date").Nullable();
                Map(x => x.top_news).Column("top_news").Nullable();
                Map(x => x.published).Column("published").Nullable();
                Map(x => x.news_link).Column("news_link").Nullable();
                Map(x => x.insert_time).Column("insert_time").Not.Nullable();
                Map(x => x.anonymous).Column("anonymous").Nullable();
                References(x => x.category_id).Column("category_id").Not.Nullable().Not.LazyLoad();
                References(x => x.country_id).Column("country_id").Nullable().LazyLoad();
                References(x => x.district_id).Column("district_id").Nullable().LazyLoad();
                References(x => x.writer).Column("writer").Nullable();
                Map(x => x.writer_name).Column("writer_name").Nullable();
                Map(x => x.content_en).Column("content_en").Nullable();
                Map(x => x.content_bn).Column("content_bn").Nullable();
                References(x => x.contents).Column("content_id").Not.LazyLoad();
                Table("news");
            }

        }
        public class contentsmap : ClassMap<bdmirrorDB.contents>
        {
            public contentsmap()
            {
                Id(x => x.content_id).Column("content_id");
                Map(x => x.bangla_content).Column("bangla_content").Nullable();
                Map(x => x.english_content).Column("english_content").Nullable();
                References(x => x.news_id).Column("news_id").Not.Nullable();
                Table("contents");
            }

        }
        public class commentsmap : ClassMap<bdmirrorDB.comments>
        {
            public commentsmap()
            {
                Id(x => x.comment_id).Column("comment_id");
                Map(x => x.content).Column("content").Not.Nullable();
                Map(x => x.email_address).Column("email_address").Not.Nullable();
                Map(x => x.com_time).Column("com_time").Not.Nullable();
                Map(x => x.is_verified).Column("is_verified").Not.Nullable();
                References(x => x.news_id).Column("news_id").Not.Nullable();
                Table("comments");
            }

        }
        public class imagesmap : ClassMap<bdmirrorDB.images>
        {
            public imagesmap()
            {
                Id(x => x.image_id).Column("image_id");
                Map(x => x.caption_bn).Column("caption_bn").Nullable();
                Map(x => x.image_name).Column("image_name").Nullable();
                Map(x => x.caption_en).Column("caption_en").Nullable();
                Map(x => x.link).Column("link").Nullable();
                Map(x => x.time).Column("time").Not.Nullable();
                Map(x => x.height).Column("height").Nullable();
                Map(x => x.width).Column("width").Nullable();
                References(x => x.news_id).Column("news_id").Nullable().LazyLoad();
                Table("images");
            }

        }
        public class advertisement_categorymap : ClassMap<bdmirrorDB.advertisement_category>
        {
            public advertisement_categorymap()
            {
                Id(x => x.add_cat_id).Column("add_cat_id");
                Map(x => x.add_cat_name).Column("add_cat_name").Nullable();
                Map(x => x.add_type).Column("add_type").Nullable();
                Map(x => x.add_position).Column("add_position").Nullable();
                Table("advertisement_category");
            }

        }
        public class advertisementsmap : ClassMap<bdmirrorDB.advertisements>
        {
            public advertisementsmap()
            {
                Id(x => x.add_id).Column("add_id");
                Map(x => x.adv_name).Column("adv_name").Not.Nullable();
                Map(x => x.adv_link).Column("adv_link").Not.Nullable();
                Map(x => x.insert_time).Column("insert_time").Nullable();
                Map(x => x.sort).Column("sort").Nullable();
                References(x => x.add_cat_id).Column("add_cat_id").Nullable();
                Table("advertisements");
            }

        }
        public class addressmap : ClassMap<bdmirrorDB.address>
        {
            public addressmap()
            {
                Id(x => x.address_id).Column("address_id");
                Map(x => x.street).Column("street").Not.Nullable();
                Map(x => x.district).Column("district").Not.Nullable();
                Map(x => x.division).Column("division").Not.Nullable();
                Table("address");
            }

        }
        public class admcountrymap : ClassMap<bdmirrorDB.admcountry>
        {
            public admcountrymap()
            {
                Id(x => x.country_id).Column("country_id");
                Map(x => x.country_name).Column("country_name").Not.Nullable();
                Map(x => x.country_code).Column("country_code").Not.Nullable();
                Map(x => x.ISDcode).Column("ISDcode").Nullable();
                Table("admcountry");
            }

        }
        public class admdivisionmap : ClassMap<bdmirrorDB.admdivision>
        {
            public admdivisionmap()
            {
                Id(x => x.division_id).Column("DivisionID");
                Map(x => x.divCode).Column("DivisionCode").Nullable();
                Map(x => x.divName).Column("DivisionName").Not.Nullable();
                Map(x => x.divCodeName).Column("DivisionCodeName").Nullable();
                References(x => x.countryid).Column("CountryID").Nullable();
                Table("admdivision");
            }

        }
        public class admdistrictmap : ClassMap<bdmirrorDB.admdistrict>
        {
            public admdistrictmap()
            {
                Id(x => x.DistrictID).Column("DistrictID");
                Map(x => x.DistrictCode).Column("DistrictCode").Nullable();
                Map(x => x.DistrictName).Column("DistrictName").Nullable();
                Map(x => x.DistrictCodeName).Column("DistrictCodeName").Nullable();
                References(x => x.DivisionID).Column("DivisionID").Nullable();
                Table("admdistrict");
            }

        }
        public class admupazilapsmap : ClassMap<bdmirrorDB.admupazilaps>
        {
            public admupazilapsmap()
            {
                Id(x => x.UpazilaPSID).Column("UpazilaPSID");
                Map(x => x.UpazilaPSCode).Column("UpazilaPSCode").Nullable();
                Map(x => x.UpazilaPSName).Column("UpazilaPSName").Nullable();
                References(x => x.DistrictID).Column("DistrictID").Nullable();
                Table("admupazilaps");
            }

        }
        public class usermap : ClassMap<bdmirrorDB.user>
        {
            public usermap()
            {
                Id(x => x.user_id).Column("user_id");
                Map(x => x.first_name).Column("first_name").Not.Nullable();
                Map(x => x.last_name).Column("last_name").Nullable();
                Map(x => x.email).Column("email").Not.Nullable();
                Map(x => x.password).Column("password").Not.Nullable();
                Map(x => x.contact_no).Column("contact_no").Not.Nullable();
                Map(x => x.gender).Column("gender").Not.Nullable();
                Map(x => x.national_id_no).Column("national_id_no").Not.Nullable();
                Map(x => x.national_id_image).Column("national_id_image").Not.Nullable();
                Map(x => x.educational_qualification).Column("educational_qualification").Not.Nullable();
                Map(x => x.occupation).Column("occupation").Not.Nullable();
                Map(x => x.writing_experience).Column("writing_experience").Not.Nullable();
                References(x => x.present_addr).Column("present_addr").Not.Nullable();
                References(x => x.permanent_addr).Column("permanent_addr").Not.Nullable();
                Table("user");
            }

        }
        public class keywordsmap : ClassMap<bdmirrorDB.keywords>
        {
            public keywordsmap()
            {
                Id(x => x.keyword_id).Column("keyword_id");
                Map(x => x.keyword).Column("keyword").Nullable();
                References(x => x.news_id).Column("news_id").Nullable();
                Table("keywords");
            }

        }
        public class keywords_bnmap : ClassMap<bdmirrorDB.keywords_bn>
        {
            public keywords_bnmap()
            {
                Id(x => x.keyword_id).Column("keyword_id");
                Map(x => x.keyword).Column("keyword").Nullable();
                References(x => x.news_id).Column("news_id").Nullable();
                Table("keywords_bn");
            }
        }
        public class current_affairsmap : ClassMap<bdmirrorDB.current_affairs>
        {
            public current_affairsmap()
            {
                Id(x => x.current_id).Column("current_id");
                Map(x => x.insert_date).Column("insert_date").Not.Nullable();
                Map(x => x.content_en).Column("content_en").Not.Nullable();
                Map(x => x.content_bn).Column("content_bn").Not.Nullable();
                Table("current_affairs");
            }

        }
        public class all_addmap : ClassMap<bdmirrorDB.all_add>
        {
            public all_addmap()
            {
                Id(x => x.add_id).Column("add_id");
                Map(x => x.title).Column("title").Not.Nullable();
                Map(x => x.image_link).Column("image_link").Not.Nullable();
                Map(x => x.image_url).Column("image_url").Nullable();
                Map(x => x.addorder).Column("addorder").Not.Nullable();
                Table("all_add");
            }

        }
        public class videosmap : ClassMap<bdmirrorDB.videos>
        {
            public videosmap()
            {
                Id(x => x.video_id).Column("video_id");
                Map(x => x.caption_en).Column("caption_en").Nullable();
                Map(x => x.caption_bn).Column("caption_bn").Nullable();
                Map(x => x.link).Column("link").Not.Nullable();
                Map(x => x.time).Column("time").Not.Nullable();
                Table("videos");
            }

        }
    }
}
