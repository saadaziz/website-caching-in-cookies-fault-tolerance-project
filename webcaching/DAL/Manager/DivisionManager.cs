﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using bdmirror24.DAL.mappings;
using bdmirror24.DAL.DALFunctions.Implementations;
namespace bdmirror24.DAL.Manager
{
    public class DivisionManager:BaseDAORepository<bdmirrorDB.admdivision>
    {
        public IList<bdmirrorDB.admdivision> GetAllDivisionByCountry(int counryid)
        {
            return (counryid > 0) ? FindAll("CountryID", counryid.ToString()) : null;
        }
        public IList<bdmirrorDB.admdivision> GetAllDivision()
        {
            return All();
        }
    }
}
