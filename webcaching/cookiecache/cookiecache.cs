﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Web;
namespace cookiecache
{
    public class cookiecache<generictype> where generictype : class
    {
        public HttpCookie add_cookie(generictype obj, string cookiename)
        {
            HttpCookie cookie = new HttpCookie("cookie_cache");
            cookie[cookiename] = obj.ToString();
            return cookie;
        }
    }
}
