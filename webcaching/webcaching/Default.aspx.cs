﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using bdmirror24.DAL.mappings;
using bdmirror24.DAL.DAL_Functions;
using bdmirror24.DAL.DAL_Functions.Implementations;
namespace webcaching
{
    public partial class _Default : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            
            //dtable.Columns.Add("date_pub");
           
            IList<int> all_news = new NewsDAO().getAllnewsint(10);
            //DateTime retriveTime = DateTime.Now;
            loadData(all_news);
        }
        protected void loadData(IList<int> all_news)
        {
            DateTime initialTime = DateTime.Now;
            rptMain.DataSource = null;
            rptMain.DataBind();
            //compression comp = new compression();
            DataTable dtable = new DataTable("news");
            dtable.Columns.Add("id");
            dtable.Columns.Add("title");
            dtable.Columns.Add("category");
            //dtable.Columns.Add("cat_link");
            //dtable.Columns.Add("title_link");
           // dtable.Columns.Add("content");
            dtable.Columns.Add("time");
            cacheHandling<bdmirrorDB.news> cachenews = new cacheHandling<bdmirrorDB.news>();
            int i = 0;
            
            foreach (int temp in all_news)
            {
                HttpCookie getCookie = Request.Cookies[temp.ToString()];
                if (getCookie == null)
                {
                    
                    bdmirrorDB.news news = new NewsDAO().pickbyID(temp);
                    string[] forsaving = new string[] { news.news_id.ToString(), news.category_id.name_en, news.title_en, news.content_en, (initialTime - DateTime.Now).ToString() };
                    //HttpCookie cookie = cachenews.add(forsaving, news.news_id.ToString());
                    //Response.Cookies.Add(cookie);
                    
                    //Response.Cookies[temp.ToString()]["content"] = forsaving[3];
                    dtable.Rows.Add(new string[] { 
                        news.news_id.ToString(),
                        news.title_en, 
                        //news.content_en, 
                        news.category_id.name_en,
                        ( DateTime.Now-initialTime ).ToString() });
                    Response.Cookies[temp.ToString()]["news_id"] = forsaving[0];
                    Response.Cookies[temp.ToString()]["category_id"] = forsaving[1];
                    Response.Cookies[temp.ToString()]["title"] = forsaving[2];
                }
                else
                {
                    //DateTime initialTime = DateTime.Now;
                    // DateTime initialTime = DateTime.Now;
                    //bdmirrorDB.news retrieve_from_cookie  = (bdmirrorDB.news) getCookie.Value;
                    //dtable.Rows.Add(new string[]
                    //Response.Write(getCookie.Value);
                    //Response.Write(getCookie["category_id"]);

                    dtable.Rows.Add(new string[] {
                        getCookie["news_id"],
                   getCookie["title"],
                    //getCookie["content"],
                         getCookie["category_id"], 
                     ( DateTime.Now-initialTime ).ToString()});


                }
                i++;
                if (i > 15) break;
            }
            rptMain.DataSource = dtable;
            rptMain.DataBind();
        }
        protected void getmore(object sender, EventArgs e)
        {
            IList<int> all_news = new NewsDAO().getAllnewsint(int.Parse(txtamount.Text));
            loadData(all_news);
        }
    }
}