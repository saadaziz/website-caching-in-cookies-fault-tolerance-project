﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Xml.Serialization;

namespace webcaching
{
    public class cacheHandling<GenericEntity> where GenericEntity : class
    {
        public HttpCookie add(string[] obj,string name)
        {
            HttpCookie res = new HttpCookie(name); 
            //StringWriter outStream = new StringWriter();
            //XmlSerializer s = new XmlSerializer(typeof(List<List<string>>));
            //s.Serialize(outStream, obj);
            res.Values.Add("news_id", obj[0]);
            res.Values.Add("category_id", obj[1]);
            res.Values.Add("title", obj[2]);
            res.Values.Add("content", obj[3]);
            res.Values.Add("time", obj[4]);


            res.Expires = DateTime.Now.AddHours(2);
            return res;
        }
    }
}