﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using bdmirror24.DAL.DAL_Functions.Implementations;
using System.Data;
using bdmirror24.DAL.mappings;

namespace webcaching
{
    public partial class withcompression : System.Web.UI.Page
    {


        protected void Page_Load(object sender, EventArgs e)
        {
            IList<int> all_news = new NewsDAO().getAllnewsint(10);
            //DateTime retriveTime = DateTime.Now;
            loadData(all_news);
        }
        protected void loadData(IList<int> all_news)
        {
            rptMain.DataSource = null;
            rptMain.DataBind();
            //compression comp = new compression();
            DataTable dtable = new DataTable("news");
            dtable.Columns.Add("id");
            dtable.Columns.Add("title");
            dtable.Columns.Add("category");
            //dtable.Columns.Add("cat_link");
            //dtable.Columns.Add("title_link");
            //dtable.Columns.Add("content");
            dtable.Columns.Add("time");
            cacheHandling<bdmirrorDB.news> cachenews = new cacheHandling<bdmirrorDB.news>();
            int i = 0;
            foreach (int temp in all_news)
            {
                
                HttpCookie getCookie = Request.Cookies["comp"+temp.ToString()];
                if (getCookie == null)
                {
                    DateTime initialTime = DateTime.Now;
                    bdmirrorDB.news news = new NewsDAO().pickbyID(temp);
                    string[] forsaving = new string[] { news.news_id.ToString(), news.category_id.name_en, news.title_en, news.content_en, (initialTime - DateTime.Now).ToString() };
                    //HttpCookie cookie = cachenews.add(forsaving, news.news_id.ToString());
                    //Response.Cookies.Add(cookie);
                    Response.Cookies["comp" + temp.ToString()]["news_id"] = forsaving[0];
                    Response.Cookies["comp" + temp.ToString()]["category_id"] = forsaving[1];
                    Response.Cookies["comp" + temp.ToString()]["title"] = string.Join(",", compression.Compress(forsaving[2]));
                    //Response.Cookies["comp" + temp.ToString()]["content"] = forsaving[3];
                    dtable.Rows.Add(new string[] { 
                        news.news_id.ToString(),
                        news.title_en, 
                        //news.content_en, 
                        news.category_id.name_en,
                        ( DateTime.Now-initialTime ).ToString() });
                }
                else
                {
                    DateTime initialTime = DateTime.Now;
                    // DateTime initialTime = DateTime.Now;
                    //bdmirrorDB.news retrieve_from_cookie  = (bdmirrorDB.news) getCookie.Value;
                    //dtable.Rows.Add(new string[]
                    //Response.Write(getCookie.Value);
                    //Response.Write(getCookie["category_id"]);
                    string[] strarr = getCookie["title"].Split(',');
                    List<string> intlst = strarr.ToList();

                    dtable.Rows.Add(new string[] {
                        getCookie["news_id"],
                   compression.Decompress(intlst),
                    //getCookie["content"],
                         getCookie["category_id"], 
                     ( DateTime.Now-initialTime ).ToString()});


                }
                i++;
                if (i > 10) break;
            }
            rptMain.DataSource = dtable;
            rptMain.DataBind();
        }
        protected void getmore(object sender, EventArgs e)
        {
            IList<int> all_news = new NewsDAO().getAllnewsint(int.Parse(txtamount.Text));
            loadData(all_news);
        }
    }
}